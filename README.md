# Life of Rust

This is a WebAssembly Rust project aimed at implementing [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life).
The idea is to have all of the business logic in a wasm module that is compiled from Rust, while the front-end is rendered in JavaScript.

## Thanks

Thanks to the [template](https://github.com/rustwasm/wasm-pack-template/), this was relatively straight-forward to set up.
