import init, { Universe } from './pkg/life_of_rust.js';

async function run() {
  await init();
  const universe = Universe.new();
  const canvas = document.getElementById("life-canvas");
  const playPauseButton = document.getElementById("play-pause");

  const CELL_SIZE = 10;
  const width = universe.width();
  const height = universe.height();
  universe.set_canvas_id("life-canvas");

  let animationId = null;

  const isPaused = () => {
    return animationId === null;
  };

  const play = () => {
    playPauseButton.textContent = "⏸";
    runLoop();
  };

  const pause = () => {
    playPauseButton.textContent = "▶";
    cancelAnimationFrame(animationId);
    animationId = null;
  };

  playPauseButton.addEventListener("click", event => {
    if (isPaused()) {
      play();
    } else {
      pause();
    }
  });

  canvas.addEventListener("click", event => {
    const boundingRect = canvas.getBoundingClientRect();

    const scaleX = canvas.width / boundingRect.width;
    const scaleY = canvas.height / boundingRect.height;

    const canvasLeft = (event.clientX - boundingRect.left) * scaleX;
    const canvasTop = (event.clientY - boundingRect.top) * scaleY;

    const row = Math.min(Math.floor(canvasTop / (CELL_SIZE + 1)), height - 1);
    const col = Math.min(Math.floor(canvasLeft / (CELL_SIZE + 1)), width - 1);

    universe.toggle_cell(row, col);
    universe.draw();
  });

  const runLoop = () => {
    universe.draw();
    universe.tick();
    animationId = requestAnimationFrame(runLoop);
  };
  play();
};

run();
